import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import Home from './screens/Home';
import AppMemo from './AppMemo';
import AppRef from './AppRef';
import AppReactMemo from './AppReactMemo';
import ApiCalls from './ApiCalls';
import AppHooks from './AppHooks';
import AppContext from './AppContext';
import AppUncontrolled from './AppUncontrolled';
import AppControlled from './AppControlled';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <ApiCalls/>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
