import React, { useState, useEffect, useCallback, useContext } from 'react';

const Home = (props) =>{

    const [counter, setCounter]= useState(0);

    useEffect(()=>{
        console.log('hello','home')
        return () => {
            console.log('Home unmounts')
        }
    },[])

    useEffect(()=>{
        console.log('Counter increased')
    },[counter])

    const increaseCounter = () =>{
        setCounter(counter+1)
    }

    const goToAbout =() =>{
        props.history.push('/about')
    }

    return (

        <div>
            <div>Home</div>
            <div>{counter}</div>
            <button onClick={increaseCounter} >+</button>
            <button onClick={goToAbout} >Go to About page</button>
        </div>
    )
}

export default Home

//React Hooks

//class & functional-stateless

//What is state?

//Rules of hooks:-
//We can't call hooks inside loops,conditions, or nested functions. They should always be at the top level of your React function.
//We can call hooks only in React function components

//dependency array