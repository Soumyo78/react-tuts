import React, { useState } from 'react';
import Descriptor from './component/Descriptor';
import { ColorContext } from './Context/context';

const AppContext = () => {
    const [color, setColor]= useState('red');

    return (
        <ColorContext.Provider value={color} >
            <Descriptor />
        </ColorContext.Provider>
    )
}

export default AppContext;

//AppContext->Descriptor->Paragraph->SmallerPara