import React from 'react';

const Movie=({title, releaseDate})=>{
    return (
        <>
        <div>{title}</div>
        <div>{releaseDate}</div>
        </>    
    )
}

const moviePropsCheck=(prevProp,nextProp)=>{
    return prevProp.title === nextProp.title;
}

export const MemoMovie=React.memo(Movie, moviePropsCheck)

export default class AppReactMemo extends React.Component{
    render(){
        return (
            <>
                <MemoMovie title='A' releaseDate="B" />
                <MemoMovie title='A' releaseDate="C" />
            </>
        )
    }
}