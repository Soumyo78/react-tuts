import React from 'react';
import useWindowDimensions from './Hooks/useWindowDimensions';

const AppHooks = () => {
    const { dimensions } = useWindowDimensions();
    console.log(dimensions)
    return (
        <div>Hello</div>
    )
}

export default AppHooks;