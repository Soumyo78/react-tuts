 import React, { Component, createRef } from "react";

 class AppRef extends Component {
   textInput = createRef();

   focusTextInput = () => {
       console.log(this.textInput);
       this.textInput.current.style.backgroundColor='red';
   }

   render() {
     return (
       <>
         <input type="text"  />
         <button ref={this.textInput} onClick={this.focusTextInput}>Focus the text input</button>
       </>
     );
   }
 }

 export default AppRef;

// import React, { useRef } from "react";

// const AppRef = () => {
//   const textInput = useRef();

//   const focusTextInput = () => textInput.current.focus();

//   return (
//     <>
//       <input type="text" ref={textInput} />
//       <button onClick={focusTextInput}>Focus the text input</button>
//     </>
//   );
// }

// export default AppRef;