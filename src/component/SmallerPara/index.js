import React from 'react';
import { ColorContext } from '../../Context/context';

class SmallerPara extends React.Component{ 
    
    
    // const contextObj=React.useContext(ColorContext); 
    // console.log(contextObj)
   render(){
    return (
        <ColorContext.Consumer>
            {
                color => (
                    <div style={{color:color}}  >
                        hello
                    </div> 
                )
            }
        </ColorContext.Consumer>
        
    )
}
}

export default SmallerPara;