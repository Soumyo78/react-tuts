import React from 'react';
import './index.css'
class Cars extends React.Component{
    constructor(props){
        super(props);
        this.state={
            carName:'',
            show:false
        }
    }
    componentDidMount(){
        console.log('component has mounted')
    }
    componentWillUnmount(){

    }
    componentDidUpdate(prevState,prevProps){
        
        if(prevState.carName==this.state.carName){
            console.log('state is updating')
        }
    }
    takeUserInput = (e) => {
        this.setState({
            carName:e.target.value
        },()=>{
            console.log(this.state.carName)
        })
    }
    showCarName = () => {
        this.setState({
            show:true
        })
    }
    render(){
        let car="Baleno";

        return (
            <div>
                <input type="text" placeholder="Type car name" onChange={this.takeUserInput} />
                <button id="submit" onClick={this.showCarName} className="showButton" >Show</button>
                {this.state.show? <div>{this.state.carName}</div>:null}
                <div className="hellocontainer">
                     <div>parent</div> 
                     <div className="hello" >hello</div>
                </div>
            </div>
        )
    }
}

export default Cars;