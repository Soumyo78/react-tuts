import React, { useCallback, useEffect, useState } from 'react';

const ApiCalls = () =>{
    const [totalArticles, setTotalArticles]=useState([])
    const [countryCOde, setCountryCode] =useState('us')

    useEffect(()=>{
        const url=`https://newsapi.org/v2/top-headlines?source=google-news&country=${countryCOde}&apiKey=a9b320f1f47644f99dab6b9407db90b0`;
       fetch(url,{
            method:'GET'
        })
        .then(res=>res.json())
        .then(res=>{

            setTotalArticles(res.articles)
            console.log(res)
        })
        .catch(err=>{
            console.log(err)
        })

    },[countryCOde])


    const changeCountryCode=(code)=>{
        setCountryCode(code);
    }
    return (



        <div>
            <button onClick={()=>changeCountryCode('in')} >India</button>
            {totalArticles && <div>Hello</div>}
        </div>
    )
}

export default ApiCalls;

//fetch returns a Promise